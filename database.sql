-- ALL the below example is based on user id = 1 
-- friends list pending request  
SELECT u.id, u.nickname, f.accepted 
    FROM users u, friendlist f 
    WHERE 
        (f.userid='1' OR f.friendId='1') AND 
        (f.userid = u.id OR f.friendId = u.id) AND 
        u.id <> '1' AND accepted = 1

-- friend list accepted list
SELECT u.id, u.nickname, f.accepted 
    FROM users u, friendlist f 
    WHERE 
        (f.userid='1' OR f.friendId='1') AND 
        (f.userid = u.id OR f.friendId = u.id) AND 
        u.id <> '1' AND accepted = 0

-- friendlist others 
SELECT id, nickname 
    FROM users 
    WHERE  
        id NOT IN  (
            SELECT u.id 
                FROM users u, friendlist f 
                WHERE 
                    (f.userid='1' OR f.friendId='1') AND
                    (f.userid = u.id OR f.friendId = u.id) 
                    AND u.id <> '1'
        ) 
        AND id<> '1'
