<?php session_start(); 
if(!isset($_SESSION['user_id']))
    echo "<script>window.location='http://localhost:8000/login.php';</script>";
$user_id = $_SESSION['user_id'];
require_once('./models/friend.php');
// get all users 
$friend = new Friend;
$notFirends =$friend->getOthers($user_id);
// get friend requests 
$friendRequest = $friend->getRequests($user_id);
// get accept requests
$accepted = $friend->getAccepted($user_id);
?>
<!DOCTYPE html>
<html>
<head>
	<title>Profile</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" 
    integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/profile.css">
</head>
<body class="bg-primary">
  <h1>Accepted</h1>
  <ul>
    <?php 
      if($accepted)
        foreach($accepted as $row){ ?>
      <li><?php echo $row['nickname'] ?></li>
        <?php }?>
  </ul>
  <h1>request</h1>
  <ul>
  <?php 
      if($friendRequest)
        foreach($friendRequest as $row){ ?>
      <li><?php echo $row['nickname'] ?></li>
        <?php }?>
  </ul>
  <h1>others</h1>
  <ul>
  <?php 
    if($notFirends)
      foreach($notFirends as $row) { ?>
      <li><?php echo $row['nickname'] ?></li>
      <?php }?>
  </ul>
  <!-- Profile picture Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-center" id="exampleModalLabel">Upload Profile picture</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
       <form action="upload.php" method="POST" enctype="multipart/form-data">
        <div class="modal-body">
          <div class="form-group">
            <img src="../uploads/<?php echo $_SESSION['profile_pic']; ?>" onclick="previmg()" id="displayimage" />
            <input type="text" name="userid" style="display: none" value="<?php echo $_SESSION['id'] ?>">
            <input type="file" class="form-control" onchange="selectedimg(this)" id="selectphoto" style="display: none" name="photo">
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" name="upload" class="btn btn-primary">Upload</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </form>
    </div>
  </div>
</div>



<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript" src="assets/js/profile.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script>
  $("#numofusers button").click(function() {
     var litext = $(this).text();
     var ret = litext.replace('add friend','');
     sessionid = "<?php echo $_SESSION['id'] ;?>";
     $.ajax({
        url: 'addfriend.php',
        type: 'POST',
        dataType: "json",
        data: {
            name: ret,
            userid: sessionid
        }
    }).done(function(data){
            alert(JSON.stringify(data));
    });
    swal("Friend request sent!");
 });

</script>
<script>
  $("#friendrequest .acpt").click(function() {
     var rqst = $(this).text();
     var acpt = rqst.replace('Accept','');
     sessionid = "<?php echo $_SESSION['id'] ;?>";
     $.ajax({
        url: 'accept.php',
        type: 'POST',
        dataType: "json",
        data: {
            name: acpt,
            userid: sessionid
        }
    }).done(function(data){
            alert(JSON.stringify(data));
    });
    swal("Friend request accepted!");
 });

</script>
<script>
  $("#friendrequest .rjct").click(function() {
    var rqst = $(this).text();
     var rjct = rqst.replace('Reject','');
     sessionid = "<?php echo $_SESSION['id'] ;?>";
     $.ajax({
        url: 'reject.php',
        type: 'POST',
        dataType: "json",
        data: {
            name: rjct,
            userid: sessionid
        }
    }).done(function(data){
            alert(JSON.stringify(data));
    });
    swal("Friend request rejected!");
 });
</script>
</body>
</html>

