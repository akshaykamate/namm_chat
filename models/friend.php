<?php 
require_once 'config/database.php';
class Friend extends Database {
    private $conn;
    function __construct() {
        $this->conn = new Database;
    }
    /*
    * Request Status 
    * 0. accepted 
    * 1. pending 
    * 2. rejected
    */
    function getRequests($user_id){
        return $this->conn->select('users u, friendlist f ', 'u.id, u.nickname, f.accepted', "(f.userid='$user_id' OR f.friendId='$user_id') AND (f.userid = u.id OR f.friendId = u.id) AND u.id <> '$user_id' AND accepted = 1");
    }
    function getAccepted($user_id){
        return $this->conn->select('users u, friendlist f ', 'u.id, u.nickname, f.accepted', "(f.userid='$user_id' OR f.friendId='$user_id') AND (f.userid = u.id OR f.friendId = u.id) AND u.id <> '$user_id' AND accepted = 0");
    }
    function getOthers($user_id){
        return $this->conn->select('users', 'id, nickname', "id NOT IN  (SELECT u.id FROM users u, friendlist f WHERE (f.userid='$user_id' OR f.friendId='$user_id') AND (f.userid = u.id OR f.friendId = u.id) AND u.id <> '$user_id') AND id<> '$user_id'" );
    }
}